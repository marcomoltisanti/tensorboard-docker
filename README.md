This repository provides an image to run tensorboard inside a container and a script to run it.
The script will pull the latest-gpu version of the tensorflow docker image, then it will run the container.
The tensorflow image will be downloaded each time an update is available.

```
./tensorflow-docker logdir [container_name]
	logdir will be mounted in /workspace in the container
    container_name (if set) will be the docker container name
```
To access tensorboard, type in the address bar of your browser *host*:6006, where *host* is the ip address or dns name of the machine hosting the docker container.